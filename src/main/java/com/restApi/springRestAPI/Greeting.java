package com.restApi.springRestAPI;

public record Greeting(long id, String content) { }
